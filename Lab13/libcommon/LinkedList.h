/*
 * LinkedList.h
 *
 *  Created on: Feb 26, 2016
 *      Author: bkubiak
 */

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_

#include <iostream>
using namespace std;
#include "Node.h"
#include "LinkedListIterator.h"

template<class T>
class LinkedList {
private:
	Node<T>* head;
	int length;
	Node<T>* getNode(int position);
public:
	LinkedList();
	virtual ~LinkedList();
	int size();
	bool add(int position, T* element);
	T* remove(int position);
	T* get(int position);
	bool set(int position, T* element);

	LinkedListIterator<T>* getIterator();
};

template<class T>
LinkedListIterator<T>* LinkedList<T>::getIterator() {
	return new LinkedListIterator<T>(head);
}

template<class T>
LinkedList<T>::LinkedList() {
	head = NULL;
	length = 0;
}

template<class T>
LinkedList<T>::~LinkedList() {
	Node<T>* nodeForRemoval = head;
	while (nodeForRemoval != NULL) {
		head = head->getNext();
		nodeForRemoval->setNext(NULL);
		delete nodeForRemoval;
		nodeForRemoval = NULL;
		nodeForRemoval = head;
	}
}

template<class T>
Node<T>* LinkedList<T>::getNode(int position) {
	Node<T>* node = head;
	for (int var = 0; var < position; ++var) {
		node = node->getNext();
	}
	return node;
}

template<class T>
int LinkedList<T>::size() {
	return length;
}

template<class T>
bool LinkedList<T>::add(int position, T* element) {
	if (element == NULL) {
		return false;
	}
	bool notValidPosition = (position < 0 || position > length);
	if (notValidPosition) {
		return false;
	}
	Node<T>* newNode = new Node<T>(element);
	if (position == 0) {
		newNode->setNext(head);
		head = newNode;
	} else {
		Node<T>* precedingNode = getNode(position - 1);
		newNode->setNext(precedingNode->getNext());
		precedingNode->setNext(newNode);
	}
	++length;
	return true;
}

template<class T>
T* LinkedList<T>::remove(int position) {
	bool notValidPosition = (position < 0 || position >= length);
	if (notValidPosition) {
		return NULL;
	}
	Node<T>* nodeForRemoval = NULL;
	if (position == 0) {
		nodeForRemoval = head;
		head = head->getNext();
	} else {
		Node<T>* precedingNode = getNode(position - 1);
		nodeForRemoval = precedingNode->getNext();
		precedingNode->setNext(nodeForRemoval->getNext());
	}
	T* removedElement = nodeForRemoval->getElement();
	nodeForRemoval->setNext(NULL);

	delete nodeForRemoval;
	nodeForRemoval = NULL;
	--length;
	return removedElement;
}

template<class T>
T* LinkedList<T>::get(int position) {
	bool notValidPosition = (position < 0 || position >= length);
	if (notValidPosition) {
		return NULL;
	}
	Node<T>* node = getNode(position);
	return node->getElement();
}

template<class T>
bool LinkedList<T>::set(int position, T* element) {
	if (element == NULL) {
		return false;
	}
	bool notValidPosition = (position < 0 || position >= length);
	if (notValidPosition) {
		return false;
	}
	Node<T>* node = getNode(position);
	node->setElement(element);
	return true;
}

#endif /* LINKEDLIST_H_ */
