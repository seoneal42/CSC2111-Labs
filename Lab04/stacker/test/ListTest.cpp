#include <iostream>
using namespace std;
#include "gtest/gtest.h"
#include "../src/LinkedList.h"

TEST(LinkedList, addToEmptyListAtValidPosition) {
	LinkedList<int> myList;
	int myInt = 13;
	bool added = myList.add(0, &myInt);
	EXPECT_TRUE(added);

	EXPECT_EQ(myList.size(), 1);
}

TEST(LinkedList, addToEmptyListAtInvalidPosition) {
	LinkedList<int> myList;
	int myInt = 23;
	bool added = myList.add(2, &myInt);
	EXPECT_FALSE(added);

	EXPECT_EQ(myList.size(), 0);
}

TEST(LinkedList, addTo3ElemListAtValidPosition) {
	LinkedList<int> myList;
	int myInt1 = 12;
	bool added = myList.add(0, &myInt1);
	EXPECT_TRUE(added);
	int myInt2 = 25;
	added = myList.add(1, &myInt2);
	EXPECT_TRUE(added);
	int myInt3 = 51;
	added = myList.add(2, &myInt3);
	EXPECT_TRUE(added);
	int myInt4 = 17;
	added = myList.add(1, &myInt4);
	EXPECT_TRUE(added);

	EXPECT_EQ(myList.size(), 4);

	EXPECT_EQ(*myList.get(0), 12);
	EXPECT_EQ(*myList.get(1), 17);
	EXPECT_EQ(*myList.get(2), 25);
	EXPECT_EQ(*myList.get(3), 51);
}

TEST(LinkedList, addTo3ElemListAtInvalidPosition) {
	LinkedList<int> myList;
	int myInt1 = 121;
	bool added = myList.add(0, &myInt1);
	EXPECT_TRUE(added);
	int myInt2 = 251;
	added = myList.add(1, &myInt2);
	EXPECT_TRUE(added);
	int myInt3 = 511;
	added = myList.add(2, &myInt3);
	EXPECT_TRUE(added);
	int myInt4 = 171;
	added = myList.add(4, &myInt4);
	EXPECT_FALSE(added);

	EXPECT_EQ(myList.size(), 3);

	EXPECT_EQ(*myList.get(0), 121);
	EXPECT_EQ(*myList.get(1), 251);
	EXPECT_EQ(*myList.get(2), 511);
}

TEST(LinkedList, removeFromEmptyList) {
	LinkedList<int> myList;
	int* removedElement = myList.remove(1);
	EXPECT_EQ(NULL, removedElement);
}

TEST(LinkedList, removeFrom1ElemList) {
	LinkedList<int> myList;
	int myInt1 = 56;
	myList.add(0, &myInt1);
	int* removedElement = myList.remove(0);
	//EXPECT_NE(NULL, removedElement);
	EXPECT_EQ(myList.size(), 0);
}

TEST(LinkedList, removeFrom3ElemListFromValidPosition) {
	LinkedList<int> myList;
	int myInt1 = 121;
	bool added = myList.add(0, &myInt1);
	EXPECT_TRUE(added);
	int myInt2 = 32;
	added = myList.add(1, &myInt2);
	EXPECT_TRUE(added);
	int myInt3 = 22;
	added = myList.add(2, &myInt3);
	EXPECT_TRUE(added);

	EXPECT_EQ(myList.size(), 3);

	bool removed = myList.remove(0);
	EXPECT_TRUE(removed);

	EXPECT_EQ(myList.size(), 2);

	EXPECT_EQ(*myList.get(0), 32);
	EXPECT_EQ(*myList.get(1), 22);
}

TEST(LinkedList, mergeFrom2ElmListTo3ElmList){
	LinkedList<int> list1;
	int myInt1 = 123;
	int myInt2 = 29;
	int myInt3 = 39;
	bool added = list1.add(list1.size(), &myInt1);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt2);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt3);
	EXPECT_TRUE(added);

	LinkedList<int> list2;
	int myInt4 = 538;
	int myInt5 = 20;
	added = list2.add(list2.size(), &myInt4);
	EXPECT_TRUE(added);
	added = list2.add(list2.size(), &myInt5);
	EXPECT_TRUE(added);

	bool merged = list1.merge(&list2);

	EXPECT_EQ(list1.size(), 5);
	EXPECT_EQ(*list1.get(0), myInt1);
	EXPECT_EQ(*list1.get(1), myInt4);
	EXPECT_EQ(*list1.get(2), myInt2);
	EXPECT_EQ(*list1.get(3), myInt5);
	EXPECT_EQ(*list1.get(4), myInt3);
}

TEST(LinkedList, mergeFromEmptyListTo3ElmList){
	LinkedList<int> list1;
	int myInt1 = 123;
	int myInt2 = 29;
	int myInt3 = 39;
	bool added = list1.add(list1.size(), &myInt1);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt2);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt3);
	EXPECT_TRUE(added);

	LinkedList<int> list2;

	bool isMerged = list1.merge(&list2);

	EXPECT_FALSE(isMerged);
	EXPECT_EQ(list1.size(), 3);
}

TEST(LinkedList, rotateElmsInListBy3){
	LinkedList<int> list1;
	int myInt1 = 123;
	int myInt2 = 29;
	int myInt3 = 39;
	int myInt4 = 37;
	bool added = list1.add(list1.size(), &myInt1);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt2);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt3);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt4);

	list1.rotate(3);

	EXPECT_EQ(*list1.get(0), myInt4);
	EXPECT_EQ(*list1.get(1), myInt1);
	EXPECT_EQ(*list1.get(2), myInt2);
	EXPECT_EQ(*list1.get(3), myInt3);
}

TEST(LinkedList, rotateElmsInListBySize){
	LinkedList<int> list1;
	int myInt1 = 123;
	int myInt2 = 29;
	int myInt3 = 39;
	bool added = list1.add(list1.size(), &myInt1);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt2);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt3);
	EXPECT_TRUE(added);

	list1.rotate(list1.size());

	EXPECT_EQ(*list1.get(0), myInt1);
	EXPECT_EQ(*list1.get(1), myInt2);
	EXPECT_EQ(*list1.get(2), myInt3);
}

TEST(LinkedList, reverseListWith5Elms){
	LinkedList<int> list1;
	int myInt1 = 123;
	int myInt2 = 29;
	int myInt3 = 39;
	int myInt4 = 1923;
	int myInt5 = 50;
	bool added = list1.add(list1.size(), &myInt1);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt2);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt3);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt4);
	EXPECT_TRUE(added);
	added = list1.add(list1.size(), &myInt5);
	EXPECT_TRUE(added);

	list1.reverse();

	EXPECT_EQ(*list1.get(0), myInt5);
	EXPECT_EQ(*list1.get(1), myInt4);
	EXPECT_EQ(*list1.get(2), myInt3);
	EXPECT_EQ(*list1.get(3), myInt2);
	EXPECT_EQ(*list1.get(4), myInt1);
}

TEST(LinkedList, reverseListWith0Elms){
	LinkedList<int> list1;

	list1.reverse();

	EXPECT_EQ(list1.size(), 0);
}


int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
