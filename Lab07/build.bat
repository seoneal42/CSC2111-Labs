@echo off
cls

set DRIVE_LETTER=%1:
set PATH=%DRIVE_LETTER%\MinGW\bin;%DRIVE_LETTER%\MinGW\msys\1.0\bin;%DRIVE_LETTER%\MinGW\gtkmm3\bin;%DRIVE_LETTER%\MinGW\gtk\bin;c:\Windows;c:\Windows\system32
set INC_DIRS = %DRIVE_LETTER%\programs\gtest-1.7.0\include
set LIB_DIRS = %DRIVE_LETTER%\programs\gtest-1.7.0\lib

g++ -c -Ix:\programs\gtest-1.7.0\include test\ListTest.cpp UnitTest\ListTest.o

g++ -Lx:\programs\gtest-1.7.0\lib -o UnitTest\ListTest.exe UnitTest\ListTest.o -lgtest