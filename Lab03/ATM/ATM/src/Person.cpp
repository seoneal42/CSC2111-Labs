/*
 * Person.cpp
 *
 *  Created on: Jan 23, 2015
 *      Author: bkubiak
 */

#include "Person.h"

Person::Person(string& newLastName, string& newPassword) {
	lastName = newLastName;
	password = newPassword;
	account = new Account();
}

Person::~Person() {
	//Done:0 THIS object creates new object dynamically - delete it!
	delete account;
	account = NULL;
}

string& Person::getLastName() {
	return lastName;
}

void Person::setLastName(string& newLastName) {
	lastName = newLastName;
}

string& Person::getPassword() {
	return password;
}

void Person::setPassword(string& password) {
	this->password = password;
}

Account* Person::getAccount() {
	return account;
}

void Person::setAccount(Account* account) {
	this->account = account;
}

bool Person::equal(string& newLastName, string& newPassword) {
	if (lastName == newLastName && password == newPassword) {
		return true;
	} else {
		return false;
	}
}

void Person::displayData() {
	cout << lastName << " has " << account->getBalance()
			<< " on his/her account." << endl;
}
