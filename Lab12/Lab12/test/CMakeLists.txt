find_package(Threads REQUIRED)

include(ExternalProject)

ExternalProject_Add(
        gtest
        URL https://googletest.googlecode.com/files/gtest-1.7.0.zip
        PREFIX ${CMAKE_CURRENT_BINARY_DIR}/gtest
        INSTALL_COMMAND ""
)

add_library(libgtest IMPORTED STATIC GLOBAL)
add_dependencies(libgtest gtest)

ExternalProject_Get_Property(gtest source_dir binary_dir)
set_target_properties(libgtest PROPERTIES
        "IMPORTED_LOCATION" "${binary_dir}/libgtest.a"
        "IMPORTED_LINK_INTERFACE_LIBRARIES" "${CMAKE_THREAD_LIBS_INIT}"
       # "INTERFACE_INCLUDE_DIRECTORIES" "${source_dir}/include"
)

include_directories("${source_dir}/include")

file(GLOB SRCS *.cpp)

ADD_EXECUTABLE(lab12test ${SRCS})

TARGET_LINK_LIBRARIES(lab12test
        libbst
        libgtest)
        

add_test(NAME lab12test
        COMMAND lab12test)