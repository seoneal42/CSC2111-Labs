/*
 * Account.h
 *
 *  Created on: Jan 25, 2015
 *      Author: B
 */

#ifndef ATM_SRC_ACCOUNT_H_
#define ATM_SRC_ACCOUNT_H_

class Account {
private:
	double balance;
public:
	Account();

	double getBalance();
	void setBalance(double newBalance);

	/**
	 * @param amount Amount that has to be deposited on account.
	 * @post Balance is increased by amount.
	 */
	void deposit(double amount);

	/**
	 * @param amount Amount that has to be withdrawn from deposit.
	 * @post If amount to be withdrawn from deposit is greater than
	 * balance then balance is not changed, otherwise balance is decreased by amount.
	 * @return TRUE if withdrawal was successful,
	 * e.i. the amount was subtracted from balance, FALSE otherwise.
	 */
	bool withdraw(double amount);
};

#endif /* ATM_SRC_ACCOUNT_H_ */
