#include <iostream>
#include <string>

using namespace std;

int simulate(int[], int);
int minimalMoves(string);
string canTransform(string, string);
void sort(int[], int);

int main(){
      //Simulate
      cout << "Simulate:" << endl;
      int x[] = {2,1,3,1,2};
      cout << simulate(x, 1) << endl;

      int y[] ={1,4,9,16,25,36,49};
      cout << simulate(y, 10) << endl;

      int z[] = {1,2,4,8,16,32,64,128,256,1024,2048};
      cout << simulate(z, 1) << endl;

      //Minimal Moves
      cout << "\nMinimal Moves: " << endl;

      string magical = ">><<><";
      string magical2 = "<<>>";

      cout << minimalMoves(magical) << endl;
      cout << minimalMoves(magical2) << endl;

      //Can Transform
      string cT1a= "fox";
      string cT1b= "fozx";

      string cT2a = "foon";
      string cT2b = "foon";

      string cT3a = "opdlfmvuicjsierjowdvnx";
      string cT3b = "zzopzdlfmvzuicjzzsizzeijzowvznxzz";

      string cT4a = "fox";
      string cT4b = "zfzoxzz";

      string cT5a = "topcoder";
      string cT5b = "zopzoder";

      cout << "\nCan Transform: "  << endl;
      cout << canTransform(cT1a, cT1b) << endl;
      cout << canTransform(cT2a, cT2b) << endl;
      cout << canTransform(cT3a, cT3b) << endl;
      cout << canTransform(cT4a, cT4b) << endl;
      cout << canTransform(cT5a, cT5b) << endl;

      //Sort
      cout << "Sort: " << endl;
      int sort1[] = {2, 1, 2, 1, 0};
      sort(sort1, 5);
      int sort2[] = {1, 2, 1, 2, 2, 1, 1, 0};
      sort(sort2, 8);

}

int simulate(int x[], int A){
      for(int var = 0; var < 200; var++){
            if((x[var] >= 1 && x[var] <= 1000000000) || (A >= 1 && A <= 1000000000)){
                  if(A == x[var])
                        A += x[var];
            }
      }

      return A;
}

int minimalMoves(string s){
      if (s.size() < 2 || s.size() > 50){
            return -1;
      }

      int mid = (s.size()/2);

      int count = 0;

      for(int i = 0; i < mid; i++){
            if(s[i] == '<'){
                  s[i] = '>';
                  count++;
            }
      }

      for (int i = mid; i < s.size(); i++){
            if(s[i] == '>'){
                  s[i] == '<';
                  count++;
            }
      }
      return count;
}

string canTransform(string init, string goal){
      if(init.length() > goal.length()){
            return "\"No\"";
      }

      for (int var = 0; var < goal.length(); var ++){
            if(goal[var] == 'z'){
                  init.insert(var, "z");
            }
      }

      if(init.length() > goal.length()){
            return "\"No\"";
      }else{
            return "\"Yes\"";
      }
}

void sort(int array[], int length){
      if (length == 0){
            cout << "Nothing to sort." << endl;
      }

      int sortedArray[length];

      int count = 0;
      for(int i = 0; i < 3; i++){
            for(int j = 0; j < length; j++){
                  if(array[j] == i){
                        sortedArray[count] = i;
                        count++;
                  }
            }
      }

      for(int i = 0; i < length; i++){
            cout << sortedArray[i] << ", ";
      }
      cout << endl;
}
