/*
 * Person.h
 *
 *  Created on: Jan 23, 2015
 *      Author: bkubiak
 */

#ifndef ATM_SRC_PERSON_H_
#define ATM_SRC_PERSON_H_

#include <iostream>
using namespace std;

#include "Account.h"

class Person {
private:
	string lastName;
	string password;
	Account* account;
public:
	Person(string& newLastName, string& newPassword);
	~Person();

	string& getLastName();
	void setLastName(string& newLastName);

	string& getPassword();
	void setPassword(string& password);

	Account* getAccount();
	void setAccount(Account* account);

	/**
	 * @param newLastName Last name.
	 * @param newPassword Password.
	 * @returns TRUE when last name given in the parameter
	 * is equal to the current object's last name and
	 * when password given in the parameter
	 * is equal to the current object's password.
	 */
	bool equal(string& newLastName, string& newPassword);

	void displayData();
};

#endif /* ATM_SRC_PERSON_H_ */
