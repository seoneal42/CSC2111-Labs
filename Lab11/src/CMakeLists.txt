set (SRC lab11.cpp Person.cpp QuickSort.h BucketSort.h SortDescending.h)

add_executable (lab11 ${SRC})

install(TARGETS lab11 DESTINATION Debug)