/*
 * Account.cpp
 *
 *  Created on: Jan 25, 2015
 *      Author: B
 */

#include "Account.h"

Account::Account() {
	balance = 0.0;
}

double Account::getBalance() {
	return balance;
}

void Account::setBalance(double newBalance) {
	balance = newBalance;
}

void Account::deposit(double amount) {
	//Done:1 update balance properly
	//if amount is less than 0 correction to question 2A
	if(amount < 0){
		return;
	}
	balance += amount;
}

bool Account::withdraw(double amount) {
	//Done:2 update balance properly
	if (amount < 0 || amount > balance)
		return false;

	balance -= amount;
	return true;
}
