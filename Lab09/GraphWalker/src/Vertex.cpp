/*
 * Vertex.cpp
 *
 *  Created on: Mar 22, 2015
 *      Author: bkubiak
 */

#include "Vertex.h"

Vertex::Vertex(int newValue) {
	value = newValue;
	visited = false;
	neighbors = new SortedArrayList<Vertex>();
}

Vertex::~Vertex() {
	delete neighbors;
}

int Vertex::getValue() {
	return value;
}

bool Vertex::getVisited() {
	return visited;
}

void Vertex::setVisited(bool isVisited) {
	visited = isVisited;
}

bool Vertex::addNeighbor(Vertex* newNeighbor) {
	if (newNeighbor == NULL) {
		return false;
	}
	neighbors->add(newNeighbor);
	return true;
}

SortedArrayList<Vertex>* Vertex::getNeighbors() {
	return neighbors;
}

int Vertex::compare(const Vertex& otherVertex) const {
	if (value < otherVertex.value) {
		return -1;
	} else if (value > otherVertex.value) {
		return 1;
	}
	return 0;
}

bool Vertex::operator ==(const Vertex& otherVertex) const {
	return !compare(otherVertex);
}

bool Vertex::operator <(const Vertex& otherVertex) const {
	return compare(otherVertex) < 0;
}
