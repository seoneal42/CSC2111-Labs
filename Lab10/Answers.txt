# 6
 
This function will take a parameter n, cout the value, and then call then
call itself with (n - 1). 
This will result in an infinite loop.



# 11

Enter: a=1 b= 7
Enter: a=1 b= 3
Leave: a= 1 b= 3
Leave: a= 1 b= 7
Value= 2

# 12

Enter: first = 1 last = 30
Enter: first = 1 last = 14
Enter: first = 1 last = 6
Enter: first = 4 last = 6
Leave: first = 4 last = 6
Leave: first = 1 last = 6
Leave: first = 1 last = 14
Leave: first = 1 last = 30
5

# 13

Calculates the octal of a number by doing the following:
1) Divides the number 'n' by 8 until it is 0.
2) Once zero it will display the modulus(remainder) of n / 8
  2a)It does so by recursively moving back up through the method calls 
     displaying the remainder of the quotient of n/8.

# 14

Function entered with n = 8
Function entered with n = 6
Function entered with n = 4
Function entered with n = 2
Function entered with n = 0
Function entered with n = 2
Function entered with n = 4
Function entered with n = 2
Function entered with n = 0
The value of f(8) is 27

The program will run forever if you pass any value below 0.