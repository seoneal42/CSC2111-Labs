#include <iostream>
using namespace std;
#include "gtest/gtest.h"
#include "../src/LinkedBST.h"
#include "../src/LinkedList.h"
#include "../src/TreeSort.h"

TEST(preorder, adding5ElmsToList) {
	int arr[] = {5, 2, 1, 6, 3};

	LinkedBST<int>* list = new LinkedBST<int>();
	for(int i = 0; i < 5; i++){
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}

	LinkedList<int>* preorder = list->preorder();

	int size = preorder->size();

	//Correct order
	int preorderArr[] = {5, 2, 1, 3, 6};
	for(int i = 0; i < size; i++){
		EXPECT_EQ(*preorder->get(i), preorderArr[i]);
	}

	delete list;
	delete preorder;
}

TEST(preorder, add0ElmToList){
	LinkedBST<int>* list = new LinkedBST<int>();

	bool isAdded = list->add(NULL);

	EXPECT_FALSE(isAdded);

	LinkedList<int>* preorder = list->preorder();

	bool isNULL;
	if(preorder == NULL)
		isNULL = true;
	else
		isNULL = false;

	EXPECT_TRUE(isNULL);

	delete list;
}

TEST(preorder, add1ElmToList){
	int arr[] = {3};

	LinkedBST<int>* list = new LinkedBST<int>();

	for(int i = 0 ; i < 1; i++){
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}

	LinkedList<int>* preorderList = list->preorder();

	//Correct order
	int preorderArr[] = {3};
	int size = preorderList->size();

	for(int i = 0; i < size; i++){
		EXPECT_EQ(*preorderList->get(i), preorderArr[i]);
	}

	delete list;
	delete preorderList;
}

TEST(postorder, adding5ElmsToList){
	int arr[] = {5, 2, 1, 6, 3};

	LinkedBST<int>* list = new LinkedBST<int>();
	for(int i = 0; i < 5; i++){
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}
	
	LinkedList<int>* postorder = list->postorder();
	
	int size = postorder->size();
	
	//Correct Order
	int postorderArr[] = {1, 3, 2, 6, 5};
	
	for(int i = 0; i < size; i++){
		EXPECT_EQ(*postorder->get(i), postorderArr[i]);
	}
	
	delete list;
	delete postorder;	
}

TEST(postorder, adding0ElmtoList){
	LinkedBST<int>* list = new LinkedBST<int>();
	
	EXPECT_FALSE(list->add(NULL));
	
	LinkedList<int>* postorder = list->postorder();
	
	bool isNull;
	
	if(postorder == NULL)
		isNull = true;
	else
		isNull = false;
		
	EXPECT_TRUE(isNull);
	
	delete list;
}

TEST(postorder, adding1ElmToList){
	int arr[] = {3};
	
	LinkedBST<int>* list = new LinkedBST<int>();
	
	for(int i = 0; i < 1; i++){
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}
	
	LinkedList<int>* postorder = list->postorder();
	
	//Correct order
	int postOrderArr[] = {3};
	
	int size = postorder->size();
	
	for(int i = 0; i < size; i++){
		EXPECT_EQ(*postorder->get(i), postOrderArr[i]);
	}
	
	delete list;
	delete postorder;
}

TEST(inorder, adding5ElmsToList){
	int arr[] = {5, 2, 1, 6, 3};
	
	LinkedBST<int>* list = new LinkedBST<int>();
	
	for(int i = 0; i < 5; i++){
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}
	
	LinkedList<int>* inorder = list->inorder();
	
	int size = inorder->size();
	
	//Correct order
	int inorderArr[] = {1, 2, 3, 5, 6};
	
	for(int i = 0; i < size; i++){
		EXPECT_EQ(*inorder->get(i), inorderArr[i]);
	}
	
	delete list;
	delete inorder;
}

TEST(inorder, adding0ElmToList){
	LinkedBST<int>* list = new LinkedBST<int>();
	
	EXPECT_FALSE(list->add(NULL));
	
	bool isNull;
	
	LinkedList<int>* inorder = list->inorder();
	
	isNull = (inorder == NULL);
	
	EXPECT_FALSE(inorder);
	
	delete list;
}

TEST(inorder, adding1ElmToList){
	LinkedBST<int>* list = new LinkedBST<int>();
	
	int arr[] = {3};
	
	for(int i = 0; i < 1; i++)
	{
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}
	
	LinkedList<int>* inorder = list->inorder();
	
	//Correct order
	int inorderArr[] = {3};
	
	int size = inorder->size();
	
	for(int i = 0; i < size; i++){
		EXPECT_EQ(*inorder->get(i), inorderArr[i]);
	}
	
	delete list;
	delete inorder;
}

TEST(treesort, adding5ElmsToSort){
	int arr[] = {5, 2, 1, 6, 3};

	LinkedList<int>* sortedList = TreeSort::treeSort(arr, 5);

	//Correct sorted order
	int sortedArr[] = {1, 2, 3, 5, 6};

	int size = sortedList->size();

	for(int i = 0; i < size; i++)
		EXPECT_EQ(*sortedList->get(i), sortedArr[i]);

	delete sortedList;
}

TEST(treesort, adding0ElmToSort){
	int arr[] = {};

	LinkedList<int>* sortedList = TreeSort::treeSort(arr, 0);

	bool isNull = (sortedList == NULL);

	EXPECT_TRUE(isNull);
}

TEST(treesort, adding1ElmToSort){
	int arr[] = {3};

	LinkedList<int>* sortedList = TreeSort::treeSort(arr, 1);

	int size = sortedList->size();

	//Correct sorted order
	int sortedArr[] = {3};

	for(int i = 0; i < size; i++){
		EXPECT_EQ(*sortedList->get(i), sortedArr[i]);
	}

	delete sortedList;
}

TEST(buildMinHeight, minHeightWith5Elms){
	int arr[] = {5, 2, 1, 6,  3};
	
	LinkedBST<int>* list = new LinkedBST<int>();
	
	for(int i = 0; i < 5; i++){
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}
	
	LinkedBST<int>* minHeightList = list->buildMinHeightBST();
	
	EXPECT_EQ(minHeightList->height(), 3);
	
	delete list;
	delete minHeightList;
}

TEST(buildMinHeight, minHeightWith0Elms){
	LinkedBST<int>* list  = new LinkedBST<int>();
	
	EXPECT_FALSE(list->add(NULL));
	
	LinkedBST<int>* minHeightList = list->buildMinHeightBST();
	
	bool isNull = (minHeightList == NULL);
	
	EXPECT_TRUE(isNull);
	
	delete list;
}

TEST(buildMinHeight, minHeightWith8Elms){
	int arr[] = {3, 1, 5, 2, 9, 23, 6, 8};
	
	LinkedBST<int>* list = new LinkedBST<int>();
	
	for(int i = 0; i < 8; i++){
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}
	
	LinkedBST<int>* minHeightList = list->buildMinHeightBST();
	
	EXPECT_EQ(minHeightList->height(), 4);
	
	delete list;
	delete minHeightList;
}

TEST(buildMinHeight, minHeightWith1Elm){
	int arr[] = {3};
	
	LinkedBST<int>* list = new LinkedBST<int>();
	
	for(int i = 0; i < 1; i++){
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}
	
	LinkedBST<int>* minHeightList = list->buildMinHeightBST();
	
	EXPECT_EQ(minHeightList->height(), 1);
	
	delete list;
	delete minHeightList;
}
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

