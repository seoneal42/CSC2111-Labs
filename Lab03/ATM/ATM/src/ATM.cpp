#include <iostream>
using namespace std;

#include "Person.h"
#include "FrontTerminal.h"
#include "SessionManager.h"

int main() {
	FrontTerminal c;
	char option;
	while (true) {
		cout << "Choose from the below options: " << endl;
		cout << "l - login" << endl;
		cout << "r - register" << endl;
		cout << "q - quit" << endl;

		cin >> option;
		if (option == 'l') {
			// log in a customer
			c.logIn();
		} else if (option == 'r') {
			// create an account (register a new customer)
			c.createAccount();
		} else if (option == 'q') {
			// terminate the loop
			break;
		}
	}
	return 0;
}
