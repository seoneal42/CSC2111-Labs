set(core_SRC lab13.cpp)

add_executable(lab13 ${core_SRC})

target_link_libraries(lab13
        libbst
        libcommon)

install(TARGETS lab13 DESTINATION debug)