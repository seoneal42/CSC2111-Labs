//============================================================================
// Name        : lab12.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "LinkedBST.h"
#include "LinkedList.h"
using namespace std;

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

	LinkedBST<int>* newList = new LinkedBST<int>();

	int arr[] = {5, 2, 1, 6, 3};

	for(int i = 0; i < 5; i++){
		newList->add(&arr[i]);
	}

	LinkedList<int>* preorderList = newList->preorder();
	LinkedList<int>* postOrderList = newList->postorder();
	LinkedList<int>* inorderList = newList->inorder();
	cout << "Preorder: ";
	for(int i = 0; i < preorderList->size(); i++)
		cout << *preorderList->get(i) << " ";

	cout << endl << "Postorder: ";
	for(int i = 0; i < postOrderList->size(); i++)
		cout << *postOrderList->get(i) << " ";

	cout << endl << "Inorder: ";
	for(int i = 0; i < inorderList->size(); i++)
		cout << *inorderList->get(i) << " ";

	LinkedBST<int>* minBST = newList->buildMinHeightBST();
	LinkedList<int>* preorder = minBST->preorder();

	cout << endl << "Preorder of minHightBST: ";
	for(int i = 0; i < preorder->size(); i++){
		cout << *preorder->get(i) << " ";
	}
	
	cout << "minHeightBST height: " << minBST->height() << endl;

	delete preorder;
	delete newList;
	delete preorderList;
	delete postOrderList;
	delete inorderList;
	delete minBST;

	return 0;
}
