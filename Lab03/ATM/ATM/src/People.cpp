/*
 * People.cpp
 *
 *  Created on: Nov 26, 2014
 *      Author: B
 */

#include "People.h"
#include "Person.h"

People::People(void) {
	maxLength = 100;
	people = new Person*[maxLength];
	length = 0;
}

People::~People(void) {
	delete[] people;
}

void People::resize() {
	maxLength = 2 * maxLength;
	Person** oldArray = people;
	people = new Person*[maxLength];
	for (int var = 0; var < length; ++var) {
		people[var] = oldArray[var];
	}
	delete[] oldArray;
}

void People::add(Person * newPerson) {
	//if newPerson is NULL correction to question 2B
	if (newPerson == NULL)
		return;

	bool notBigEnough = (maxLength == length);
	if (notBigEnough) {
		resize();
	}
	people[length] = newPerson;
	++length;
}

Person* People::get(int position) {
	bool notValidPosition = (position < 1 || position > length);
	if (notValidPosition) {
		return NULL;
	}
	return people[position - 1];
}

Person* People::get(string& lastName, string& password) {
	int var = 0;
	Person* person;
	while (var < length) {
		person = people[var];
		if (person->equal(lastName, password)) {
			return person;
		}
		++var;
	}
	return NULL;
}

int People::getLength() {
	return length;
}
