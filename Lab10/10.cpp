#include <iostream>

using std::cout;
using std::endl;

int sum_recursive_to_n(int [], int n);
int sum_recursive_start_and_end(int start, int end);
void write_from_one_to_n(int n);
void write_squares_from_one_to_n(int n);
void reverse_integer(int n);
void writeLine(char c, int n);
void writeBlock(char c, int n, int r);
int getValue(int a, int b, int n);
int search(int first, int last, int n);
int mystery(int n);
void displayOctal(int n);
int f(int n);

int main() {

    cout <<"Problem 3:" << endl;
    int array[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    cout << sum_recursive_to_n(array, 6) << endl;

    cout << endl << "Problem 4:" << endl;
    cout << sum_recursive_start_and_end(0, 10) << endl;

    cout << endl << "Problem 7: " << endl;
    write_from_one_to_n(10);
    cout << endl << endl;

    cout << "Problem 8: " << endl;
    write_squares_from_one_to_n(10);
    cout << endl << endl;

    cout << "Problem 9: " << endl;
    reverse_integer(345);
    cout << endl << endl;

    cout << "Problem 10a: " << endl;
    writeLine('*', 5);
    cout << endl;

    cout << "Problem 10b: " << endl;
    writeBlock('*', 5, 3);
    cout << endl;

    cout << "Problem 11: " << endl;
    cout << getValue(1, 7, 7);
    cout << endl << endl;

    cout << "Problem 12: " << endl;
    cout << mystery(30) << endl;
    cout << endl;

    cout << "Problem 13: " << endl;
    displayOctal(100);
    cout << endl << endl;

    cout << "Problem 14: " << endl;
    cout << "The value of f(8) is " << f(8) << endl;
    return 0;
}

//# 3
int sum_recursive_to_n(int loc_a[], int n) {
    if (n == 0) {
	return loc_a[0];
    }
    return sum_recursive_to_n(loc_a, (n-1)) + loc_a[n];
}

//# 4
int sum_recursive_start_and_end(int start, int end) {
    if (start < end) {
	return sum_recursive_start_and_end(start, (end - 1)) + end;
    } else {
	return start;
    }
}

//#7
void write_from_one_to_n(int n) {
    
    if (n <= 0) {
	return;
    }
    else {
	write_from_one_to_n(n - 1);
	cout << n << ' ';
    }
}

//#8
void write_squares_from_one_to_n(int n) {
    
    if (n <= 0) {
	return;
    }
    else {
	write_squares_from_one_to_n(n - 1);
	cout << (n*n) << ' ';
    }
}

//#9
void reverse_integer(int n) {//345
    if (n == 0) {
	return;
    }
    int answer = 0;
    answer += (n % 10);//5,4,3
    cout << answer;
    n /=  10;//34,3,0
    reverse_integer(n);
}

//#10
void writeLine(char c, int n) {
    if (n == 0) {
	return;
    }
    cout << c;
    writeLine(c, n-1);
}

void writeBlock(char c, int n, int r){
    if(r == 0)
        return;

    writeLine(c, n);
    cout << endl;
    writeBlock(c, n, r-1);
}

//#11
int getValue(int a, int b, int n){
    int returnValue = 0;

    cout << "Enter: a=" << a << " b= " << b << endl;
    int c = (a + b)/2;
    if(c*c <= n)
        returnValue = c;
    else
        returnValue = getValue(a, c-1, n);

    cout << "Leave: a= " << a << " b= " << b << endl;
    return returnValue;
}

//#12
int search(int first, int last, int n){
    int returnValue = 0;
    cout << "Enter: first = " << first << " last = "
    << last << endl;

    int mid = (first + last)/2;
    if( (mid*mid <= n) && (n < (mid+1) * (mid+1)))
        returnValue = mid;
    else if(mid *mid > n)
        returnValue = search(first, mid-1, n);
    else
        returnValue = search(mid+1, last, n);

    cout << "Leave: first = " << first << " last = "
    << last << endl;

    return returnValue;
}

int mystery(int n){
    return search(1, n, n);
}

//#13
void displayOctal(int n){
    if(n > 0)
    {

        if (n/8 >0) {

            displayOctal(n / 8);
        }
        cout << n % 8;

    }
}

//#14
int f(int n){
    cout << "Function entered with n = " << n << endl;
    switch(n){
        case 0: case 1: case 2:
            return n + 1;
        default:
            return f(n-2) * f(n-4);
    }
}