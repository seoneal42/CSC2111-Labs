/*
 * People.h
 *
 *  Created on: Nov 26, 2014
 *      Author: B
 */

#ifndef ATM_SRC_PEOPLE_H_
#define ATM_SRC_PEOPLE_H_

#include "Person.h"

class People {
private:
	Person** people;
	int maxLength;
	int length;

	void resize();
public:
	People();
	~People();

	/**
	 * Inserts an object of type Person at the end of the list.
	 * @param newPerson The object that has to be inserted into the list.
	 * @post An object is added at the end of the list,
	 * length of the list is increased by 1.
	 */
	void add(Person* newPerson);

	/**
	 * Gets the element at a specified position.
	 * @param position The position of the element that has to be retrieved from the list.
	 * @return NULL if there is no element at a given position
	 * (in special case - when the list is empty).
	 * Otherwise, it returns the requested element.
	 */
	Person* get(int position);

	/**
	 * @param lastName Last name.
	 * @param password Password.
	 * @returns Reference to an object of type Person
	 * whose last name is equal to the value given in the parameter lastName
	 * and password is equal to the value given in the parameter password.
	 * If there is no such person then it returns NULL.
	 */
	Person* get(string& lastName, string& password);

	int getLength();
};

#endif /* ATM_SRC_PEOPLE_H_ */
